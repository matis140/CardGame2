﻿using System;
using CardLib;
using CardLib.Interface;
using CardLib.Object;

namespace CardLibTest
{
    public static class TestConstants
    {
        ///This is a complete deck and can be used to test that generated decks are complete.
        public static ICard[] GetAFullPlayingCardDeck()
        {
            ICard[] _cards = new Card[52];
            _cards[0] = new Card(CardName.Ace, (Suit)1);
            _cards[1] = new Card(CardName.Two, (Suit)1);
            _cards[2] = new Card((CardName)3, (Suit)1);
            _cards[3] = new Card((CardName)4, (Suit)1);
            _cards[4] = new Card((CardName)5, (Suit)1);
            _cards[5] = new Card((CardName)6, (Suit)1);
            _cards[6] = new Card((CardName)7, (Suit)1);
            _cards[7] = new Card((CardName)8, (Suit)1);
            _cards[8] = new Card((CardName)9, (Suit)1);
            _cards[9] = new Card((CardName)10, (Suit)1);
            _cards[10] = new Card((CardName)11, (Suit)1);
            _cards[11] = new Card((CardName)12, (Suit)1);
            _cards[12] = new Card((CardName)13, (Suit)1);

            _cards[13] = new Card((CardName)1, (Suit)2);
            _cards[14] = new Card((CardName)2, (Suit)2);
            _cards[15] = new Card((CardName)3, (Suit)2);
            _cards[16] = new Card((CardName)4, (Suit)2);
            _cards[17] = new Card((CardName)5, (Suit)2);
            _cards[18] = new Card((CardName)6, (Suit)2);
            _cards[19] = new Card((CardName)7, (Suit)2);
            _cards[20] = new Card((CardName)8, (Suit)2);
            _cards[21] = new Card((CardName)9, (Suit)2);
            _cards[22] = new Card((CardName)10, (Suit)2);
            _cards[23] = new Card((CardName)11, (Suit)2);
            _cards[24] = new Card((CardName)12, (Suit)2);
            _cards[25] = new Card((CardName)13, (Suit)2);

            _cards[26] = new Card((CardName)1, (Suit)3);
            _cards[27] = new Card((CardName)2, (Suit)3);
            _cards[28] = new Card((CardName)3, (Suit)3);
            _cards[29] = new Card((CardName)4, (Suit)3);
            _cards[30] = new Card((CardName)5, (Suit)3);
            _cards[31] = new Card((CardName)6, (Suit)3);
            _cards[32] = new Card((CardName)7, (Suit)3);
            _cards[33] = new Card((CardName)8, (Suit)3);
            _cards[34] = new Card((CardName)9, (Suit)3);
            _cards[35] = new Card((CardName)10, (Suit)3);
            _cards[36] = new Card((CardName)11, (Suit)3);
            _cards[37] = new Card((CardName)12, (Suit)3);
            _cards[38] = new Card((CardName)13, (Suit)3);

            _cards[39] = new Card((CardName)1, (Suit)4);
            _cards[40] = new Card((CardName)2, (Suit)4);
            _cards[41] = new Card((CardName)3, (Suit)4);
            _cards[42] = new Card((CardName)4, (Suit)4);
            _cards[43] = new Card((CardName)5, (Suit)4);
            _cards[44] = new Card((CardName)6, (Suit)4);
            _cards[45] = new Card((CardName)7, (Suit)4);
            _cards[46] = new Card((CardName)8, (Suit)4);
            _cards[47] = new Card((CardName)9, (Suit)4);
            _cards[48] = new Card((CardName)10, (Suit)4);
            _cards[49] = new Card((CardName)11, (Suit)4);
            _cards[50] = new Card((CardName)12, (Suit)4);
            _cards[51] = new Card((CardName)13, (Suit)4);

            return _cards;
        }

    }
}
