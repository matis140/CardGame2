﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autofac;

using CardLib.Interface;
using CardLib.Object;

namespace CardGame
{
    internal static class Startup
    {
        internal static IContainer RegisterContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Decks>().As<IDeck>();
            builder.RegisterType<Hand>().As<IHand>();

            return builder.Build();
        }
    }
}
