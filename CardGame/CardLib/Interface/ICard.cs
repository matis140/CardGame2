﻿
namespace CardLib.Interface
{
    public interface ICard
    {
        /// <summary>
        /// Name of the card. Ace, Two, King, Nine...
        /// </summary>
        CardName Name { get; }
        /// <summary>
        /// Suit of the card.
        /// </summary>
        Suit CardSuit { get; }
        /// <summary>
        /// Get a dispaly friendly name for the card. Ace of Spades, Nine of Hearts
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        string DispalyName(DeckType type = DeckType.PlayingCards);
    }
}