﻿using System;
using System.Collections.Generic;

using CardLib.Interface;
using CardLib;
using CardLib.Exceptions;
using System.Linq;

namespace CardGame
{
    public class GameLoop
    {
        #region MemberVariables
        private IList<IHand> _players;
        private IDeck _deck;
        private int _count = 0;
        #endregion MemberVariables

        #region Constructors
        public GameLoop(IList<IHand> playerhands, IDeck deck)
        {
            _players = playerhands;
            _deck = deck;

            //Deal everyone in and then the game will start
            foreach (var hand in _players)
            {
                try
                {
                    hand.Add(_deck.Deal(5));
                }
                catch(Exception)
                {
                    //Not ever likley to hit any excpetions here. 3-6 players with 5 cards is a max of 30 delt.
                    //maybe put some loggin in here or something.
                }
            }
        }
        #endregion //Constructors

        #region PublicMethods
        public bool Loop()
        {
            bool again = true;

            if (again)
            {
                for (int i = 0; i < _players.Count; i++)
                {
                    again = PlayerTurn(_players[i], i);
                    if (!again)
                        break;
                }
            }

            _count++;
            if (_count == 100000)
            {
                again = false;
                Console.WriteLine("This game is taking too long");
            }

            if (!again)
            {
                PrintPlayerScore();
            }

            return again;
        }
        #endregion PublicMethods

        #region PrivateMethods
        private void PrintPlayerScore()
        {
            var tophand = _players.OrderByDescending(x => x.Count).First();
            var winnerIndex = _players.IndexOf(tophand);

            Console.WriteLine($"Player {winnerIndex} won with {tophand.Count} cards!");
            foreach(var c in tophand.ShowHand().OrderBy(x => x.Name))
            {
                Console.WriteLine(c.DispalyName());
            }
        }

        private bool PlayerHasCards(IHand hand)
        {
            return hand.Count > 0;
        }

        private bool PlayerTurn(IHand player, int playerindex)
        {
            bool StartTurn = !CheckGameover();
            if (StartTurn)
            {
                //pick a player and guess a random card from in your own hand
                var guess = player.RandomCardName();
                var target = PickOtherRandomPlayer(playerindex);                
                //true == continue
                var result = _players[target].Get(guess);
                if(result.Count > 0)
                    Console.WriteLine($"Player {playerindex} guessed {guess} from target {target} and got {result.Count} cards!");

                if (result.Count > 0)
                {
                    //if player got some add to hand
                    player.Add(result);
                    //success turn continues
                    StartTurn = PlayerTurn(player, playerindex);
                }
                else
                {//false == draw
                    ICard card = null;
                    try
                    {
                        card = _deck.Deal(1).First();
                    }
                    catch(DeckOutofCards)
                    {
                        StartTurn = false;
                    }

                    if (card != null)
                    {
                        player.Add(card);
                        if (card.Name == guess)
                        {
                            Console.WriteLine($"Player {playerindex} fished their wish and their turn continues!");
                            StartTurn = PlayerTurn(player, playerindex);
                        }
                    }
                }
            }
            return StartTurn;
        }

        private int PickOtherRandomPlayer(int currentPlayer)
        {
            var pick = StaticRandom.Rand(_players.Count-1);
            if (pick == currentPlayer)
            {
                pick = PickOtherRandomPlayer(currentPlayer);
            }

            return pick;
        }

        private bool CheckGameover()
        {
            bool gameover = false;
            for(int i =0; i< _players.Count; i++)
            {
                var hascards = PlayerHasCards(_players[i]);
                if(!hascards && _deck.Count() == 0)
                {
                    gameover = true;
                    Console.WriteLine($"Player {i} is out of cards and the deck is gone! Game has ended.");
                }
            }

            return gameover;
        }
        #endregion PrivateMethods
    }
}
