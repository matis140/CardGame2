﻿using System.Collections.Generic;

namespace CardLib.Interface
{
    public interface IDeck
    {       
        ///Returns requested cards. Can throw DeckNotThatBigException if dealing more cards than deck contains.
        IList<ICard> Deal(int count);
        IList<ICard> OrderCards(bool ascending = true);
        //Shuffle all decks of all cards together
        void Shuffle();
        //Count of all cards in all decks
        int Count();
    }
}