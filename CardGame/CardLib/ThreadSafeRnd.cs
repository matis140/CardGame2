﻿using System;
using System.Threading;

namespace CardLib
{
    //https://stackoverflow.com/questions/19270507/correct-way-to-use-random-in-multithread-application
    //Maybe look into RandomNumberGenerator from System.Security.Cryptography if time later.
    //Dug this out because using multiple threads to sort portions of the decks would introduce random results.
    public static class StaticRandom
    {
        static int seed = Environment.TickCount;

        static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public static int Rand()
        {
            return random.Value.Next();
        }

        public static int Rand(int max)
        {
            return random.Value.Next(max);
        }

        public static int Rand(int min, int max)
        {
            return random.Value.Next(min, max);
        }
    }
}