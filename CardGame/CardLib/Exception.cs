﻿using System;

namespace CardLib.Exceptions
{
    public class DeckOutofCards : Exception
    {
        public DeckOutofCards() : base("You have exhasted your deck!") { }
    }

    public class DeckOutOfBounds : Exception
    {
        public DeckOutOfBounds() : base("Your deck does not have that many cards!") { }
    }
}